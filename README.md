# Python

Please see the [general pipeline](https://gitlab.ethz.ch/gitlab-pipelines/general) for some additional options.

This pipeline ensures:
 - Good formatting
 - Following the PEP guidelines
 - Some security checks
 - Run some unit tests
