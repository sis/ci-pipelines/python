# isort will complain about the sorting
# flake8 will complain about the unused import
import sys
# bandit will complain about subprocess
import subprocess


class Main:
    # black and flake8 will complain here
    def __init__( self):
        # Disable bandit here
        self.process = subprocess.Popen('/bin/ls *', shell=True)  # nosec
        pass

    def fail(self):
        raise Exception()
