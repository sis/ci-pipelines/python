.ONESHELL:

install-poetry:
	pip3 install poetry

update-poetry:
	poetry update

install-libraries:
	poetry install

install-prod-libraries:
	poetry install --without=dev

test:
	set -e
	poetry run coverage run -m pytest
	poetry run coverage report
	poetry run coverage xml

bandit:
	poetry run bandit -r app

safety:
	poetry run safety check

black:
	poetry run black --check .

isort:
	poetry run isort --check-only --profile black .

flake8:
	poetry run flake8 .
